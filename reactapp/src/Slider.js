import React, { Component } from 'react';
import { Slide } from 'react-slideshow-image';
import data from './data/LijstAntwerpen.json';

// packets installeren voor de react slideshow
// npm install react-slideshow-image -S

const properties = {
  duration: 5000,
  transitionDuration: 1000,
  infinite: true,
  indicators: true,
  arrows: false
};

class Slider extends Component {
    list = data.Curiosity;
    
    
    row = this.list.map((item) => {
        let url = `/images/small/${item.image}`;
        return (
        <div className="each-slide" key={item.index}>
                <span>{item.name}</span><br/>
                <img  src={url} alt="pictures are loading"></img>

        </div>
        );
    });
    
    render() {
        return (
      <Slide {...properties}>
        {this.row}
      </Slide>
        );
    }
}

export default Slider;