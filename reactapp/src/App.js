import React, { Component } from 'react';
import './App.css';
import Home from  './Home';
import List from  './List';
import Detail from './Detail';

class App extends Component {
  
  routing = (action, data, passList) => {
      switch (action) {
        case 'Home' : 
          this.route = <Home action={this.handleClick}/>;
          break;
        case 'List' :
          
          if (typeof passList  !== 'undefined') {
            this.route = <List action={this.handleClick} list={passList}/>;
          }
          else{
            this.route = <List action={this.handleClick} />;
          }
          break;
        case 'Detail' :
          this.route = <Detail action={this.handleClick} data={data} list={passList}/>;
          break;

        default :
          this.route = <Home action={this.handleClick} />;
          break;
      }
      this.setState({ action: this.route });
  };  
  
  handleClick = (action, key, list) => {
      this.routing(action, key, list);
  }
  
  route = <Home action={this.handleClick} />

  render() {
    return (
      this.route
    );
  }
}

export default App;
